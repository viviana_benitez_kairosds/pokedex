import { TestBed } from '@angular/core/testing';

import { PokemonsStoreService } from './pokemons-store.service';

describe('PokemonsStoreService', () => {
  let service: PokemonsStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PokemonsStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
