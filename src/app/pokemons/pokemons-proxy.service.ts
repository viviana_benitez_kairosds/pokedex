import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs/internal/Observable';
import { PokemonsDTO, ProfilePokemonDTO } from './model/PokemonsDTO';

@Injectable({
  providedIn: 'root'
})
export class PokemonsProxyService {

  constructor(private http: HttpClient) { }


  getPokemonByName(namePokemon: string): Observable<ProfilePokemonDTO>{
    return this.http.get<ProfilePokemonDTO>(`https://pokeapi.co/api/v2/pokemon/${namePokemon}`)
  }

  getPokemons(): Observable<PokemonsDTO>{
    return this.http.get<PokemonsDTO>('https://pokeapi.co/api/v2/pokemon')
  }


  getNextPage(nextPage: string): Observable<PokemonsDTO> {
    return this.http.get<PokemonsDTO>(nextPage)
  }

}
