import { Injectable } from '@angular/core';
import { Store } from '../state/store';
import { ProfilePokemon } from './model/profile-pokemon';
import { PokemonsService } from './pokemons.service';


@Injectable({
  providedIn: 'root'
})
export class PokemonsStoreService extends Store<ProfilePokemon[]>{

  constructor(private pokemonsService: PokemonsService) {
    super();
  }

  init() {
    this.store([])
  }

  addPokemon(pokemon: ProfilePokemon) {
    this.store([...this.get(), pokemon]);
  }

 

  

}
