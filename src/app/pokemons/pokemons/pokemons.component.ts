import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { PokemonsDTO, ResultsDTO } from '../model/PokemonsDTO';
import { PokemonsService } from '../pokemons.service';
import 'genk-search/genk-search.js'
import { ProfilePokemon } from '../model/profile-pokemon';
import { PokemonsStoreService } from '../pokemons-store.service';

@Component({
  selector: 'app-pokemons',
  templateUrl: './pokemons.component.html',
  styleUrls: ['./pokemons.component.css']
})
export class PokemonsComponent implements OnInit {

  pokemons$?: Observable<PokemonsDTO>;
  profilePokemon$?: Observable<ProfilePokemon>;
  pokemonsStore$?: Observable<ProfilePokemon[]>;
  pokemon!: ProfilePokemon;

  constructor(private pokemonsService: PokemonsService,
              private pokemonsStoreService: PokemonsStoreService
              ) { }

  ngOnInit(): void {
    this.pokemonsStoreService.init();
    this.pokemons$ = this.pokemonsService.getPagePokemons()
    const el = document.querySelector('genk-search');
    el?.addEventListener('search:pokemon', (event: any) => this.searchPokemon(event.detail.name));
    this.pokemonsStore$ = this.pokemonsStoreService.get$()
  }

  changePage(pageNext: string){
    this.pokemons$ = this.pokemonsService.getPage(pageNext)
  }

  searchPokemon(namePokemon: string){
    this.profilePokemon$ = this.pokemonsService.getPokemonByName(namePokemon)
  }

  async addPokemonToPokedex(pokemon: Observable<ProfilePokemon>){
    pokemon.subscribe(profile => this.pokemonsStoreService.addPokemon(profile))
    console.log('store: ', this.pokemonsStore$);
    
  }


}
