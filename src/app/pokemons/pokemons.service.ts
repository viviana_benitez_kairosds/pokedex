import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { PokemonsProxyService } from './pokemons-proxy.service';
import { PokemonsDTO, ProfilePokemonDTO, ResultsDTO } from './model/PokemonsDTO';
import { ProfilePokemon } from './model/profile-pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonsService {

  constructor(private proxy: PokemonsProxyService) { }

  getPagePokemons(): Observable<PokemonsDTO> {
    return this.proxy.getPokemons().pipe(
      map((pagesPokemons: PokemonsDTO) => {
        return pagesPokemons
      })
    )
  }

  getPage(page: string): Observable<PokemonsDTO>{
    return this.proxy.getNextPage(page).pipe(
      map((pagesPokemons: PokemonsDTO) => {
        return pagesPokemons
      })
    )
  }

  getPokemonByName(namePokemon: string): Observable<ProfilePokemon>{
    return this.proxy.getPokemonByName(namePokemon).pipe(
      map((profilePokemon: ProfilePokemonDTO) => {
        return {
          name: profilePokemon.name,
          base_experience: profilePokemon.base_experience,
          height: profilePokemon.height,
          weight: profilePokemon.weight,
          types: profilePokemon.types
        }
      })
    )

  }

}
