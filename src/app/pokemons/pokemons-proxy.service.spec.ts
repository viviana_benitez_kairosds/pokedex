import { TestBed } from '@angular/core/testing';

import { PokemonsProxyService } from './pokemons-proxy.service';

describe('PokemonsProxyService', () => {
  let service: PokemonsProxyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PokemonsProxyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
