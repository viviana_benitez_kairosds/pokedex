import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PokemonsComponent } from './pokemons/pokemons.component';
import { HttpClientModule } from '@angular/common/http';

const ROUTES: Routes = [
  {
    path: 'pokemons', component: PokemonsComponent
  },
];

@NgModule({
  declarations: [PokemonsComponent],
  imports: [
    CommonModule,HttpClientModule, RouterModule.forChild(ROUTES)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PokemonsModule { }
