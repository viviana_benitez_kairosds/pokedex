import { Type } from "./PokemonsDTO";

export interface ProfilePokemon {
    name: string;
    base_experience?: number;
    height?: number;
    weight?: number;
    types: Type[];

}
