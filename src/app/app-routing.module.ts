import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { PruebaComponent } from './pokemon/prueba/prueba.component';
import { PokemonsComponent } from './pokemons/pokemons/pokemons.component';


const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'pokemon', component: PruebaComponent},
  {path: 'login', component: LoginComponent},
  {path: 'pokemons', component: PokemonsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
