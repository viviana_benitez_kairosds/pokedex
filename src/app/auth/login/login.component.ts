import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form!: FormGroup;
  authVerify?: boolean

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    })
  
  }

  login(){
    const username = this.form.get('username')?.value;
    const password = this.form.get('password')?.value;
    //validar datos a través de un servicio
    this.authVerify = this.authService.auth(username,password)
    if(this.authVerify){
      this.router.navigate(["pokemons"])
    }
  }

}
