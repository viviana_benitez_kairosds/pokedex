import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  auth(username: string, password: string): boolean {
    return username === password;
  }
}
