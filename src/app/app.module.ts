import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth/auth.module';
import { PokemonsModule } from './pokemons/pokemons.module';
import { PokemonsComponent } from './pokemons/pokemons/pokemons.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthModule,
    PokemonsModule
  ],
  providers: [],
  bootstrap: [AppComponent],

})
export class AppModule { }